<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Upload;
use App\Http\Controllers\ImobilController;
use App\Http\Controllers\ProgramareController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Route pentru userController
Route::post('register',[UserController::class,'registerUser']);
Route::post('login',[UserController::class,'login']);
Route::post('uploadPdf',[Upload::class,'addDocumentPDF']);
Route::post('uploadImobil',[ImobilController::class,'uploadImobil']);
Route::post('returnareImobile',[ImobilController::class,'returnareImobile']);
Route::get('imobil/{id}',[ImobilController::class,'getImobil']);
Route::post('uploadProgramare',[ProgramareController::class,'uploadProgramare']);
Route::get('imobile',[ImobilController::class,'getImobile']);
Route::get('programareImobil/{id}',[ProgramareController::class,'programareImobil']);
Route::get('ultimeleProgramari/{id}',[ImobilController::class,'ultimeleProgramari']);