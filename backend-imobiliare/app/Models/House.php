<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use HasFactory;

    protected $fillable = [
        'descriere',
        'suprafata',
        'telefonAgentie',
        'emailAgentie',
        'telefonProprietar',
        'emailProprietar',
        'adresa',
        'poza',
        'status',
        'tip'
        
    ];

    public $timestamps=false;
}
