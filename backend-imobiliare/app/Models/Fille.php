<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fille extends Model
{
    use HasFactory;

    protected $fillable = [
        'numeFisier',
        'continut',
        
    ];

    public $timestamps=false;
}
