<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imobil extends Model
{
    use HasFactory;

    public $table='imobils';
    protected $fillable = [
        'descriere',
        'suprafata',
        'telefonAgentie',
        'emailAgentie',
        'telefonProprietar',
        'emailProprietar',
        'adresa',
        'poza',
        'status',
        'tip'
        
    ];

    public $timestamps=false;
}
