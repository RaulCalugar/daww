<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Appointment;

use Carbon\Carbon;

use DB;

use DateTime;


class ProgramareController extends Controller
{
    // Functie prin care pun in baza de date programarile pentru un anumit imobil
    function uploadProgramare(Request $request)
    {
        
        // Convertesc inputul oferit in year-month-day hour:00:00
        $temp=date('Y-m-d H',strtotime($request->input('data')));
        // Iau data din inputul introdus
        $data=date('Y-m-d',strtotime($request->input('data')));
        // Iau ora din inputul introdus
        $ora=date('H',strtotime($request->input('data')));
        error_log($ora);
        // Vad daca pentru imobilul dat ca parametru am facuta o programare
        $status=DB::table('appointments')->where('idImobil','=',$request->input('idImobil'))->where('data','=',$data)->where('ora','=',$ora)->get();
        // Daca nu am facuta o programare, atunci salvez programarea
        if($status->isEmpty())
        {
            $programare=new Appointment;
            $programare->idImobil=$request->input('idImobil');// Imobilul la care s-a facut programarea
            $programare->idUser=$request->input('idUser');// Userul care a facut programarea
            $programare->data=$data;// Data la care s-a facut programarea -> year-month-day
            $programare->ora=$ora;//Ora la care s-a facut programarea: {ora}
            
            $programare->save();
            
            return response()->json(['mesaj'=>'succes']);
        }

      
        // Daca este rezervata o vizita la o data si data la un imobil, vad cand pot face alta
        $cauta=true;

        $val=new Carbon($request->input('data'));// Folosit pentru a adauga cate o ora la fiecare noua cautare
        
        while($cauta==true)
        {
            //Mai adaug o ora la intrare
            
            $val->addHour();
            $str=$val->format('Y-m-d H');// Convertesc valoarea de tip datetime pentru a putea face interogarea
            $data=$val->format('Y-m-d');
            $ora=$val->format('H');
            
            // Vad daca la urmatoarea ora sunt rezultate disponibile
            $status=DB::table('appointments')->where('idImobil','=',$request->input('idImobil'))->where('data','=',$data)->where('ora','=',$ora)->get();
            // Daca urmatoarea ora este libera, atunci fac o sugestie
            if($status->isEmpty()){
                return response()->json(['mesaj' => $str]);
            }
        }
        return $status;
        
       

    }

    // Iau toate programarile de la un anumit imobil
    function programareImobil($id){
        
        return Appointment::select('data','ora')->where('idImobil',$id)->get();

    }
}
