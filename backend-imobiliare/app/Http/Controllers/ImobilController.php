<?php

namespace App\Http\Controllers;

use DB;

use Illuminate\Http\Request;

use App\Models\Imobil;

use App\Models\House;

use Carbon\Carbon;

use Carbon\CarbonPeriod;

class ImobilController extends Controller
{
    //Incarc un imobil in baa de date
    function uploadImobil(Request $request)
    {
        $imobil=new House;
        $imobil->descriere=$request->input('descriere');
        $imobil->suprafata=$request->input('suprafata');
        $imobil->telefonAgentie=$request->input('telefonAgentie');
        $imobil->emailAgentie=$request->input('emailAgentie');
        $imobil->telefonProprietar=$request->input('numarTelefonProprietar');
        $imobil->emailProprietar=$request->input('emailProprietar');
        $imobil->adresa=$request->input('adresa');
        //Calea unde este stocata poza
        $imobil->poza=$request->file('poza')->store('poze');
        $imobil->status=$request->input('status');
        $imobil->tip=$request->input('tip');
        $imobil->save();
        


        return $imobil;
    }

    //Returnare imobile pentru pagina de profil a utilizatorului
    function returnareImobile(Request $request)
    {
       
        $status=$request->input('status');
        $tipuri=$request->input('fav');
        // $rez=DB::table('houses')->where('status','=',$status)->get();
        $rez=DB::table('houses')->where('status','=',$status)->whereIn('tip',$tipuri)->get();
        return $rez;
        
    }

    //Returnez imobilul dupa id
    function getImobil($id)
    {
        
        return House::find($id);
    }

    // Returnez toate imobilele (asta pentru rolul de agent)
    function getImobile()
    {
        return House::all();
    }

  

    // Returnez numarul programarilor din ultimele 30 de zile
    function ultimeleProgramari($id)
    {
        $final=Carbon::now();
        $final=$final->addDay();
        $start=Carbon::now()->subDays(30);
        
        $dates = [];
        // Merg de la ultimele 30 de zile pana la ziua de azi si vad cate programari am
        for($date = $start->copy(); $date->lessThan($final); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }

        // Pun rezultatul sub forma zi|numarProgramari 
        $result=[];
        foreach($dates as $data){
        
            $numar=DB::table('appointments')->where('idImobil','=',$id)->where('data','=',$data);// Numarul de programari
            $temp=$data."|".$numar->count();
            array_push($result,$temp);
        }
        return $result;
       
    }
}
