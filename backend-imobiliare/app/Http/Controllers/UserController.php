<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

use DB;

// Folosit la encriptare parole
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    //Inregistrez un nou utilizator
    function registerUser(Request $req)
    {
        $temp=DB::table('users')->where('nume','=',$req->input('nume'))->get();
        // Daca mai exista un user cu numele dat, se va afisa un mesaj
        if(!$temp->isEmpty())
        {
            return response()->json(['mesaj'=>'exista']);
        }
        //Salvez noul user creat
        $user=new User;
        $user->nume=$req->input('nume');
        $user->email=$req->input('email');
        $user->tip=$req->input('rol');
        // Encriptarea parolei
        $user->parola=Hash::make($req->input('parola'));

        //Salvez userul create in baza de date
        $user->save();

        return $user;
    }

    //Ma loghez cu un nou utilizator
    function login(Request $request)
    {
        //Caut utilizatorul dupa nume
        $user=User::where('nume',$request->nume)->first();

        if(!$user || !Hash::check($request->parola,$user->parola)){
            return response()->json(['mesaj'=>'eroare']);
        }

        return $user;

    }
}
