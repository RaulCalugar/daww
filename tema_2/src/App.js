import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import Acasa from './Pagini/Acasa';
import Stiri from './Pagini/Componente/Stiri';
import Contact from './Pagini/Contact';
import Despre from './Pagini/Despre';
import Inchirieri from './Pagini/Inchirieri';
import Inregistrare from './Pagini/Inregistrare';
import Logare from './Pagini/Logare';
import Upload from './Pagini/Upload';
import Vanzari from './Pagini/Vanzari';
import UploadImobil from './Pagini/UploadImobil';
import Profil from './Pagini/User/Profil';
import Statistica from './Pagini/Componente/Statistica';
import RuteProtejate from './Pagini/Componente/RuteProteajate';

function App() {
  return (
    <>
    <Router>
      <div>

      
      <Routes>
        <Route path='/' element={<Acasa/>}/>
        <Route path='/noutati' element={<Stiri/>}/>
        <Route path='/despreNoi' element={<Despre/>}/>
        <Route path='/inchirieri' element={<Inchirieri/>}/>
        <Route path='/vanzari' element={<Vanzari/>}/>
        <Route path='/contact' element={<Contact/>}/>
        <Route path='/logare' element={<Logare/>}/>
        <Route path='/register' element={<Inregistrare/>}/>

        {/* Rutele protejate */}
        {/* Pagina upload pdf */}
        <Route exct path='/upload'  element={<RuteProtejate cmp={Upload}/>}>
        </Route>
        {/* Pagina upload imobil pentru agent */}
        <Route  path='/uploadImobil' element={<RuteProtejate cmp={UploadImobil}/>} >
          
        </Route>
        {/* Pagina profil utilizator logat */}
        <Route  path='/profil' element={<RuteProtejate cmp={Profil}/>}>
          
        </Route>
        <Route  path='/statistica'  element={<RuteProtejate cmp={Statistica}/>}>
        
        </Route>
      </Routes>
      </div>
    </Router>

    
      
      
      
    
    </>
  );
}

export default App;
