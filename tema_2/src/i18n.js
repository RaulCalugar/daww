import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import translationEN from './limbi/en/translationEN.json'
import translationRO from './limbi/ro/translationRO.json'


const resources={
    en:{
        translation:translationEN
    },
    ro:{
        translation:translationRO
    }
};

i18n.use(initReactI18next).init({
    resources,lng:localStorage.getItem('limba'),
    keySeparator:false,
    interpolation:{
        escapeValue:false
    }
});

export default i18n



