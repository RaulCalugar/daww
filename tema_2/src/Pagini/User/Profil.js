import React, { useState, useEffect } from 'react'
import Checkbox from "@material-ui/core/Checkbox";
import InputLabel from "@material-ui/core/InputLabel";
import ListItemText from "@material-ui/core/ListItemText";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import OutlinedInput from "@material-ui/core/OutlinedInput"
import Table from "@material-ui/core/Table"
import 'date-fns';




import { DropDown } from '../Componente/Dropdown'
import '../Styles/Profil.css'
import ModalLocuinta from '../Componente/ModalLocuinta'
import { HOST } from '../../links'
import Meniu from '../Meniu';
import ModalProgramari from '../Componente/ModalProgramari';
import Programari from '../Componente/Programari';


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const optiuni = [
    'garsoniera',
    'apartament',
    'casa'
];



export default function Profil() {

    // Vad ce fel de user am
    let user=JSON.parse(localStorage.getItem('userLogat'))

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    // De vanzare sau inchiriere
    const [status, setStatus] = useState('')
    //Selectii (garsoniera, apartament,casa)
    const [selectii, setSelectii] = useState([]);
    // Imobilele selectate pe care userul le vrea
    const [imobile, setImobile] = useState([]);
    // Selectarea datei pentru programare
    const [selectedDate, setSelectedDate] = useState(new Date());

    // Toate imobilele pe care un agent le poate vedea
    const[list,setList]=useState([])

    // Imobilul la care vreau sa fac programare

    // Schimbarea datei pentru planificare
    const handleDateChange = (date) => {
        console.log(date);
        setSelectedDate(date);
    };
    
    const handleChange = (event) => {
        const {
            target: { value },
        } = event;
        setSelectii(

            typeof value === 'string' ? value.split(',') : value,
        );
    };

    // Selectez locuintele de interes pentru un utilizator
    async function apasa() {

        const fav = selectii
        const body = { fav, status }
        
        await fetch(HOST.backend_api + 'returnareImobile', {
            method: 'POST',
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body),
        })
            .then(response => response.json())
            .then(text => { setImobile(text); console.log(text) });

    }

    // Returnez toate imobilele pentru rolul de agent sa vad la fiecare imobil programarea
    async function listAll(){

        let res=await fetch(HOST.backend_api+'imobile');
        res=await res.json();
        setList(res);
        console.log("RESS:\n",res)

    }


   

    return (
        <>
            <Meniu/>
            <h1 style={{color:'red', textAlign:'center'}}>Pagina Profil {user.nume}</h1>
            <h1 style={{color:'yellow', textAlign:'center'}}>Tip User: {user.tip}</h1>
            <h1 style={{color:'blue', textAlign:'center'}}>Email: {user.email}</h1>
            <br />
            <br />
            <br />
            <br />
            <div>
                
                <div className='tipuri'>

                
                {/* DropDown in care am tipurile de locuinta */}
                <FormControl className='form'>
                    <InputLabel id="demo-multiple-checkbox-label">Tiprui</InputLabel>
                    <Select
                        labelId="demo-multiple-checkbox-label"
                        id="demo-multiple-checkbox"
                        multiple
                        value={selectii}
                        onChange={handleChange}
                        input={<OutlinedInput label="Tipuri" />}
                        renderValue={(selected) => selected.join(', ')}
                        MenuProps={MenuProps}
                    >
                        {optiuni.map((name) => (
                            <MenuItem key={name} value={name}>
                                <Checkbox checked={selectii.indexOf(name) > -1} />
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
               
                {/* DropDown in care am statusul imobilului */}
                <DropDown style={{ position: "relative", left: "10px" }}
                    values={['Vanzare', 'Inchiriere']}
                    multiple={false} name={'Status'}
                    setValues={(selected) => setStatus(selected)} />
                </div>
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <button className="submit" onClick={apasa}>Vizualizare</button>
                <br />
                
                {/* Tabel in care afisez toate tipurile de imobile pe care utilizatorul le vrea */}
                <Table>
                    <tr>
                        <td>Tip</td>
                        <td>Status</td>
                        <td>Suprafata</td>
                        <td>Adresa</td>
                        <td>Imagine</td>
                        <td>Programare</td>
                    </tr>
                    {
                        imobile.map((item) =>
                            <tr>
                                <td>{item.tip}</td>
                                <td>{item.status}</td>
                                <td>{item.suprafata}</td>
                                <td>{item.adresa}</td>
                                <td> <img style={{ width: 200 }} src={"http://localhost:8000/" + item.poza} /></td>
                                <td><ModalLocuinta imobil={item} /></td>
                            </tr>


                        )
                    }
                     
                </Table>
                <br />
                <br />
                <br />
                <br />

                {/* Partea unde sunt date pentru un agent imobiliar */}

                {
                    user.tip==='Agent' ? 
                    <>
                    <button className="programari" onClick={listAll}>Programari</button>

                    <Table>
                        <tr>
                            <td>Tip</td>
                            <td>Status</td>
                            <td>Suprafata</td>
                            <td>Adresa</td>
                            <td>Imagine</td>
                            <td>Programari</td>
                            <td>Grafic</td>
                        </tr>
                        {
                            list.map((item) =>
                                <tr>
                                    <td>{item.tip}</td>
                                    <td>{item.status}</td>
                                    <td>{item.suprafata}</td>
                                    <td>{item.adresa}</td>
                                    <td> <img style={{ width: 200 }} src={"http://localhost:8000/" + item.poza} /></td>
                                    <td><ModalProgramari idImobil={item.id}/></td>
                                    <td><Programari idImobil={item.id} /> </td>
                                </tr>
    
    
                            )
                        }
                         
                    </Table> 
                    </>:null
                }
                

            </div>

            
        </>
    );
}
