import React from "react";
import './Styles/Inchirieri.css'
export default function Inchirieri()
{
    return(
        <>
            <h1 className="titluInchirieri">
        Pagina Inchirieri Imobiliare
    </h1>
    {/* <!-- Partea unde sunt garsoniere de inchiriat --> */}
    <div id="garsoniere">
        <h1 className="titluGarsoniere">Vedeti Lista Garsonierelor de Inchiriat</h1>  
        {/* <!-- Anunturi garsoniere de inchiriat --> */}
        <div className="anunt">
       
            <img src={require('./Imagini/Garsoniere/g1.jpg')} alt=""  className="imagineAnunt"/>
            <div className="continutAnunt">
                <h1 className="autor">Autor:Ion Popescu</h1>
                <p className="descriereAnunt">Inchiriez garsoniera de 11 mp in Cluj-Napoca, strada Zambilei</p>
            </div>
        </div>
        <div className="anunt">
            
            <img src={require('./Imagini/Garsoniere/g2.jpeg')} alt=""   className="imagineAnunt"/>
            <div className="continutAnunt">
                <h1 className="autor">Autor:Ion Popescu</h1>
                <p className="descriereAnunt">Inchiriez garsoniera de 25 mp in Cluj-Napoca, strada Obsevatorului</p>
            </div>
        </div>
    </div>


    {/* <!-- Partea unde sunt apartamente de inchiriat --> */}
    <div id="apartamente">
        <h1 class="titluApratamente">
            Vedeti lista apartamentelor de inchiriat
        </h1>
        {/* <!-- Anunturi apartamente de inchiriat --> */}
        <div class="anunt">
            <img src="./Imagini/Apartamente/a1.jpg" alt="" className="imagineAnunt" />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez apartament in Busteni. Detalii la numarul de telefon 0711223344</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Apartamente/a2.jpeg" alt="" className="imagineAnunt"  />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez apartament in Busteni. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Apartamente/a3.jpg" alt="" className="imagineAnunt"  />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez apartament in Busteni. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
    </div>

    {/* <!-- Partea unde sunt case de inchiriat --> */}
    <div id="case">
        <h1 class="titluCase">
            Vedeti Lista Caselor de Inchiriat
        </h1>
        <div class="anunt">
            <img src="./Imagini/Case/c1.jpg" alt="" className="imagineAnunt"  />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Case/c2.jpeg" alt="" className="imagineAnunt"  />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Case/c3.jpeg" alt="" className="imagineAnunt"  />
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Inchiriez casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>

    </div>

        </>
    )
}

