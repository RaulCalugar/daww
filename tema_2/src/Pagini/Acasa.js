import React from 'react'
import Meniu from './Meniu'
import './Styles/Acasa.css'
import {useTranslation} from 'react-i18next'

function Acasa() {

  const{t,i18n}=useTranslation();
  return (
    <div>
      <div>
        <Meniu/>
      </div>
       
          {/* <p class="intro">
         
        Firma noastra de imobiliare ofera cele mai bune servicii. Suntem foarte atenti la nevoile clientilor nostri.
       
         Locatiile noastre sunt urmatoarele:
        </p>  */}
        <div>
        <p class='intro'>{t('infoHome')}</p>
        </div>
        
    <ul>
        <li class="locatie">Bucuresti</li>
        <li class="locatie">Ilfov</li>
        <li class="locatie">Iasi</li>
        <li class="locatie">Cluj-Napoca</li>
        <li class="locatie">Timisoara</li>

    </ul>

    </div>
  )
}

export default Acasa