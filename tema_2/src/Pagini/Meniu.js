import React, { useState } from "react";
import './Styles/Meniu.css'
import { useTranslation } from 'react-i18next'
import { Menu, MenuItem, Button } from '@material-ui/core'
import Translator from "./Componente/Translator";
import { useNavigate } from "react-router-dom";

export default function Meniu() {

    const navigate=useNavigate()
    const [anchorEl, setAnchorEl] = useState(null);
    const open = Boolean(anchorEl);
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
        setAnchorEl(null);
    };
    const { t, i18n } = useTranslation();

    function logout(){
        localStorage.removeItem('userLogat')
        navigate('/')
    }

    const profil=()=>{
        navigate('/profil')
    }

    const uploadPDF=()=>{
        navigate('/upload')
    }

    const uploadImobil=()=>{
        navigate('/uploadImobil')
    }

    let user=JSON.parse(localStorage.getItem('userLogat'))
    
    return (
        <>
            <div id="container">
                <header className="headerMeniu">

                    <img class="imagineLogo" src="./Imagini/logoFirma.png" alt="" />

                    <nav>
                        <ul id="meniuPrincipal">
                            <li><a href="./"> {t('acasaMeniu')}</a></li>
                            <li><a href="./noutati">{t('noutatiMeniu')}</a></li>
                            <li><a href="./despreNoi">{t('despreNoi')}</a></li>
                            <li><a href="./inchirieri">{t('inchirieriMeniu')}</a>
                                <ul>
                                    <li><a href="./Inchirieri#garsoniere">{t('garsoniere')}</a></li>
                                    <li><a href="./Inchirieri#apartamente">{t('apartamente')}</a></li>
                                    <li><a href="./Inchirieri#case">{t('case')}</a></li>
                                </ul>
                            </li>
                            <li><a href="./Vanzari">{t('vanzariMeniu')}</a>
                                <ul>
                                    <li><a href="./Vanzari#garsoniere">{t('garsoniere')}</a></li>
                                    <li><a href="./Vanzari#apartamente">{t('apartamente')}</a></li>
                                    <li><a href="./Vanzari#case">{t('case')}</a></li>
                                </ul>
                            </li>
                            {
                                // Vedem daca avem utilizator logat
                                // Daca este logat afisez un buton mai special unde poate merge in diverse pagini
                                localStorage.getItem('userLogat') ?
                                    <>
                                    <li  >
                                        <div className='userMenu'>
                                            <Button
                                                id="basic-button"
                                                aria-controls={open ? 'basic-menu' : undefined}
                                                aria-haspopup="true"
                                                aria-expanded={open ? 'true' : undefined}
                                                onClick={handleClick}
                                                style={{backgroundColor:"#9CC920"}}
                                                
                                            >
                                                User
                                            </Button>
                                            <Menu
                                                id="basic-menu"
                                                anchorEl={anchorEl}
                                                open={open}
                                                onClose={handleClose}
                                                MenuListProps={{
                                                    'aria-labelledby': 'basic-button',
                                                }}
                                            >
                                                <MenuItem onClick={profil}>Profil</MenuItem>
                                                <MenuItem onClick={uploadPDF}>Upload PDF</MenuItem>
                                                <MenuItem onClick={logout}>Logout</MenuItem>
                                                {
                                                    user.tip==='Agent' ? 
                                                    <MenuItem onClick={uploadImobil}>UploadImobil</MenuItem> : null
                                                }
                                                
                                            </Menu>
                                        </div>
                                        </li>

                                        
                                       
                                    </> :
                                    // Daca nu este logat afisam link catre paginile de login si register
                                    <>
                                        <li><a href="./logare"> Login</a></li>
                                        <li><a href="./register"> Register</a></li>
                                    </>
                            }

                            <li><a href="./Contact">{t('contactMeniu')}</a></li>
                        </ul>
                        <Translator />
                    </nav>


                </header>
            </div>
        </>
    )
}