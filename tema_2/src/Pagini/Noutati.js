import React from 'react'
import './Styles/Noutati.css'

function Noutati({stire}) {

  const poza=stire.children[0].firstChild.data
  const titlu=stire.children[1].firstChild.data
  const continut=stire.children[2].firstChild.data
  
  return (
    <>
        <div class="paginaNoutati">
            
            <br/>
            <div class="container">
                <div class="cardStire">
                    {/* <!-- Imaginea stirii --> */}
                    <img class="imagineStire" src={poza}/>

                    <div class="detaliiStire">
                        {/* <!-- Div unde este pus numele stirii --> */}
                        <div class="numeStire">
                            {titlu}
                            {/* <!-- Paragraful care contine textul stirii --> */}
                            <p class="continutStire">
                                {continut}
                                {/* <iframe src="./Stire1.txt" frameborder="0" width="90%"></iframe> */}
                            </p>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

       
    
    
    </>
  )
}

export default Noutati