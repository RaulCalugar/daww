import React, { useState, useEffect } from 'react'
import Container from "@material-ui/core/Container";
import { Grid } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { DropDown } from './Componente/Dropdown';
import { HOST } from '../links';
import { useNavigate } from 'react-router-dom'
const asezare = {
    top: "20px"
}



function Inregistrare() {

    const [nume, setNume] = useState("");
    const [parola, setParola] = useState("");
    const [email, setEmail] = useState("");
    const [rol, setRol] = useState("");//Tip user:agent sau client
    const navigate = useNavigate();

    // Vedem daca utilizatorul este logat, atunci numai poate accesa pagina de 
    // register si merge la pagina de acasa
    useEffect(() => {
        if (localStorage.getItem('userLogat')) {
            navigate('/')
        }
    }, [])

    // Buton prin care trimit datele la back-end
    async function onSubmitFun() {


        const body = { nume, parola, email, "rol": rol[0] }
        
        let res = await fetch(HOST.backend_api + 'register', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        })
        
        res = await res.json();
        if(res.mesaj==='exista'){
            alert("Exista deja un utilizator cu acest nume")
        }
        else{
            alert("Cont creat cu succes")
            //Pun pe localStorage pentru a vedea ca am utilizator logat
            localStorage.setItem("userLogat", JSON.stringify(res))
            //Merg la pagina de home
            navigate('/')
        }
        
        

    }

    const handleChange = (event) => {
        console.log(event)
        setRol(event.target.name);
    };
    return (
        <>
            <h1>Inregistrare nou utilizator</h1>
            <Container maxWidth="xs" style={asezare}>

                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth

                                    label="Nume"
                                    name="nume"
                                    size="small"
                                    variant="outlined"
                                    onChange={e => setNume(e.currentTarget.value)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    label="Parola"
                                    name="parola"
                                    size="small"
                                    type="password"
                                    variant="outlined"
                                    onChange={e => setParola(e.currentTarget.value)}

                                />

                            </Grid>

                            <Grid item xs={12}>
                                <TextField
                                    fullWidth
                                    label="Email"
                                    name="email"
                                    size="small"
                                    type="email"
                                    variant="outlined"
                                    onChange={e => setEmail(e.currentTarget.value)}
                                    className="inputEmail"
                                />
                            </Grid>

                            <DropDown style={{ position: "relative", left: "10px" }} 
                            values={['Agent', 'Client']} 
                            multiple={false} name={'Rol'} 
                            setValues={(selected) => setRol(selected)} />


                        </Grid>
                    </Grid>
                    <Grid item xs={12}>
                        <Button color="secondary" fullWidth type="submit" variant="contained" onClick={onSubmitFun}>
                            INREGISTRARE
                        </Button>
                    </Grid>
                </Grid>


            </Container>
        </>
    )
}

export default Inregistrare