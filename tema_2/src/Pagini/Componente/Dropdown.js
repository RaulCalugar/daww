import { FormControl, InputLabel, MenuItem, OutlinedInput, Select, SelectChangeEvent, useTheme } from "@material-ui/core";
import { useState } from "react";
import React from "react";

export const DropDown = ({values,name,multiple,setValues}) => {

    
    const [personName, setPersonName] = useState([]);

    const handleChange = (event) => {
        const { target: { value }, } = event;
        setPersonName(typeof value === 'string' ? value.split(',') : value,)
        setValues(typeof value === 'string' ? value.split(',') : value,);
    };

    return (
        <FormControl sx={{ m: 1 }} style={{width:"400px", left:"5px"}} >
            <InputLabel style={{left:"5px"}} id="demo-multiple-name-label">{name}</InputLabel>
            <Select
                labelId="demo-multiple-name-label"
                id="demo-multiple-name"
                multiple={multiple}
                value={personName}
                onChange={handleChange}
                defaultValue={values}
                input={<OutlinedInput label={name} />}
                
            >
                {values?.map((name) => (
                    <MenuItem
                        key={name}
                        value={name}
                        
                    >
                        {name}
                    </MenuItem>
                ))}
            </Select>
        </FormControl>
    )
}