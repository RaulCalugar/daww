import React,{useState,useEffect} from 'react'
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import Modal from "@material-ui/core/Modal"
import { Button } from '@material-ui/core'
import {HOST} from '../../links.js'
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDateTimePicker
} from '@material-ui/pickers';


export default function ModalImobil({idImobil}) {

    const [open, setOpen] = React.useState(true);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const [selectedDate, setSelectedDate] = useState(new Date());

    const handleDateChange = (date) => {
        console.log(date);
        setSelectedDate(date);
    };

    useEffect(()=>{
        // alert(idImobil)
        // handleOpen()
        // let res= fetch(HOST.backend_api+'imobil/'+idImobil)
        // res= res.json()
        // console.log('res:\n',res)
    },[])

    return (
        <>
            <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
          <Button onClick={handleOpen}>Open modal</Button>
                <Box sx={style}>
                    <Typography id="keep-mounted-modal-title" variant="h6" component="h2">
                        Programare Vizita Imobil
                    </Typography>
                    {/* Descriere */}
                    <div style={{ display: 'flex' }}>
                        <h1>Descriere:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Suprafata */}
                    <div style={{ display: 'flex' }}>
                        <h1>Suprafata:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Email Agentie */}
                    <div style={{ display: 'flex' }}>
                        <h1>emailAgentie:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Telefon Agentie */}
                    <div style={{ display: 'flex' }}>
                        <h1>telefonAgentie:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Telefon Proprietar */}
                    <div style={{ display: 'flex' }}>
                        <h1>telefonProprietar:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Email Proprietar */}
                    <div style={{ display: 'flex' }}>
                        <h1>emailProprietar:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Adresa */}
                    <div style={{ display: 'flex' }}>
                        <h1>adresa:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Status */}
                    <div style={{ display: 'flex' }}>
                        <h1>Status:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    {/* Tip */}
                    <div style={{ display: 'flex' }}>
                        <h1>Tip:</h1>
                        <h1>mfmfsgmfsl;gfsmdg</h1>
                    </div>
                    <div>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>

                            <KeyboardDateTimePicker
                                id="time-picker"
                                label="Time picker"
                                value={selectedDate}
                                onChange={handleDateChange}
                            />

                        </MuiPickersUtilsProvider>
                    </div>

                </Box>
                </Modal>
        </>
    )
}



const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

