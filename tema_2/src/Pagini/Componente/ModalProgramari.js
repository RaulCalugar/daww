import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, TableBody } from '@material-ui/core'
import { styled, Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Typography } from "@material-ui/core"
import Timeline from 'react-calendar-timeline'
// make sure you include the timeline stylesheet or the timeline will not be styled
import 'react-calendar-timeline/lib/Timeline.css'
import moment from 'moment'
import {HOST} from '../../links.js'

export default function ModalProgramari({idImobil}) {


    const [open, setOpen] = React.useState(false);
    const[programari,setProgramari]=useState([]);// Programarile unui anumit imobil
    const zero=":00:00"
    const handleClickOpen = () => {
        setOpen(true);

        listProgramari()
    };

    const inchidere = () => {
        setOpen(false)
    }

    async function handleClose() {

    }

    async function listProgramari(e){
        
        e.preventDefault()
        setOpen(true);
        let res=await fetch(HOST.backend_api + 'programareImobil/'+idImobil, {
            method: 'GET',
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },
            
        })
            .then(response => response.json())
            .then(text => { setProgramari(text);  });
       
        
    }



    const BootstrapDialog = styled(Dialog)(({ theme }) => ({
        '& .MuiDialogContent-root': {
            padding: theme.spacing(2),
        },
        '& .MuiDialogActions-root': {
            padding: theme.spacing(1),
        },
    }));

    const BootstrapDialogTitle = (props) => {
        const { children, onClose, ...other } = props;

        return (
            <DialogTitle sx={{ m: 10, p: 22 }} {...other}>
                {children}
                {onClose ? (
                    <IconButton
                        aria-label="close"
                        onClick={onClose}
                        sx={{
                            position: 'absolute',
                            right: 18,
                            top: 18,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        {/* <CloseIcon /> */}
                    </IconButton>
                ) : null}
            </DialogTitle>
        );
    };

    BootstrapDialogTitle.propTypes = {
        children: PropTypes.node,
        onClose: PropTypes.func.isRequired,
    };

    return (
        <>
            <Button variant="outlined" onClick={listProgramari}>
                Programari
            </Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    ID Imobil:{idImobil}
                </BootstrapDialogTitle>
                <DialogContent dividers style={{width:"500px"}}>
                   {programari.map(programare=><h1>{programare.data} : {programare.ora} {zero} </h1>)}
                </DialogContent>
                <DialogActions>
                   
                    <Button autoFocus onClick={inchidere}>
                        Inchidere
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </>
    )





}