import React,{useState,useEffect} from "react";
import { useNavigate } from 'react-router-dom';

export default function RuteProtejate(props){
    const navigate=useNavigate()
    let Comp=props.cmp;
    useEffect(()=>{

        // console.log("USER:\n",localStorage.getItem('userLogat').nume)
        if(!localStorage.getItem('userLogat')){
            navigate('/logare')
        }
    },[])
    return(
        <>
            <Comp/>
        </>
    )

}