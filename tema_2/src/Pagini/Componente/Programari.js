import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, TableBody } from '@material-ui/core'
import { styled, Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Typography } from "@material-ui/core"
import { HOST } from '../../links.js'
import { Line } from 'react-chartjs-2';
import { useNavigate } from 'react-router-dom'

export default function Programari({idImobil}) {
    const [open, setOpen] = React.useState(false);
    const [zile, setZile] = useState([]);// Zilele la care s-au facut diverse programari
    const [numere, setNumere] = useState([]);// Numarul de programari pentru o anumita zi
    const navigate=useNavigate();
    let days=[]
    let count=[]
    const inchidere = () => {
        setOpen(false)
    }

    const hours=["00", "01", "02", "03", "04"]
    const values=[1,2,3,4]
    async function handleClose() {

    }
    async function graficProgramari() {

        
        navigate('/statistica', {
            state: {
              userId: idImobil,
            }
          });
       
    }



    const BootstrapDialog = styled(Dialog)(({ theme }) => ({
        '& .MuiDialogContent-root': {
            padding: theme.spacing(2),
        },
        '& .MuiDialogActions-root': {
            padding: theme.spacing(1),
        },
    }));

    const BootstrapDialogTitle = (props) => {
        const { children, onClose, ...other } = props;

        return (
            <DialogTitle sx={{ m: 10, p: 22 }} {...other}>
                {children}
                {onClose ? (
                    <IconButton
                        aria-label="close"
                        onClick={onClose}
                        sx={{
                            position: 'absolute',
                            right: 18,
                            top: 18,
                            color: (theme) => theme.palette.grey[500],
                        }}
                    >
                        {/* <CloseIcon /> */}
                    </IconButton>
                ) : null}
            </DialogTitle>
        );
    };

    BootstrapDialogTitle.propTypes = {
        children: PropTypes.node,
        onClose: PropTypes.func.isRequired,
    };

    return(
        <>
            <Button variant="outlined" onClick={graficProgramari}>
                Grafic
            </Button>
            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >
                <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
                    ID Imobil:{idImobil}
                </BootstrapDialogTitle>
                <DialogContent dividers style={{width:"500px"}}>
                {/* <Line
            data={{labels:hours,
                datasets:[{data:values,
                    
                backgroundColor:
                [
                    "#3cd111",
                    "#0000FF",
                    "#9966FF",
                    "#4C4CFF",
                    "#00FFFF",  
                ],
                }]
                ,}}
               
                title="Measured Values"
       /> */}
                <h1>ldmflds</h1>
                </DialogContent>
                <DialogActions>
                   
                    <Button autoFocus onClick={inchidere}>
                        Inchidere
                    </Button>
                </DialogActions>
            </BootstrapDialog>
        </>
    )
}