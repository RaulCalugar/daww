import React,{useState} from 'react'
import i18n from '../../i18n'
import '../Styles/Translator.css'

function Translator() {

  

  const schimbareLimba=(e)=>{
    localStorage.setItem('limba',e.currentTarget.value)
    i18n.changeLanguage(localStorage.getItem('limba'))
  }
  return (
    <div className='translator'>
      <select name='limba' id='limba' value={localStorage.getItem('limba')} onChange={schimbareLimba}>
        <option value='ro'>RO</option>
        <option value='en'>EN</option>  
        
      </select>

    </div>
  )
}

export default Translator