import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import { Button, TableBody } from '@material-ui/core'
import { styled, Dialog, DialogTitle, DialogContent, DialogActions, IconButton, Typography } from "@material-ui/core"
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDateTimePicker
} from '@material-ui/pickers';
import { HOST } from '../../links'
import { format } from 'date-fns'
import "../Styles/ModalLocuinta.css"
export default function ModalLocuinta({ imobil }) {

  const [open, setOpen] = React.useState(false);
  const [selectedDate, setSelectedDate] = useState(new Date());

  let user=JSON.parse(localStorage.getItem('userLogat'))

  const handleClickOpen = () => {
    setOpen(true);
  };

  const inchidere = () => {
    setOpen(false)
  }

  // Efectuez programarea pentru un anumit imobil
  async function handleClose() {

    // Formatez data selectata pentru a o trimite la back-end
    const data = format(selectedDate, 'yyyy/MM/dd kk:mm:ss')
    console.log("PP:",data)
    // Obiectul pe care il trimit catre back-end
    const body = {
      'data': data,//Data care s-a selectat
      'idImobil': imobil.id.toString(),//Locuinta la care se face programare
      'idUser': user.id//Userul care face programarea 
    }

    await fetch(HOST.backend_api + 'uploadProgramare', {
      method: 'POST',
      headers: {
        'Accept': '*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body),
    })
      .then(response => response.json())
      .then(text => { 
        console.log(text.mesaj);
        if(text.mesaj==='succes'){
          alert("Programare facuta cu succes")
       
        }
        else{
          const msj=text.mesaj
          alert("Cea mai apropiata ora de programare este: "+msj)
        }
      });

  };





  // Selectez data aleasa de user
  const handleDateChange = (date) => {
    
    setSelectedDate(date);
  };

  const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
      padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
      padding: theme.spacing(1),
    },
  }));

  const BootstrapDialogTitle = (props) => {
    const { children, onClose, ...other } = props;

    return (
      <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
        {children}
        {onClose ? (
          <IconButton
            aria-label="close"
            onClick={onClose}
            sx={{
              position: 'absolute',
              right: 8,
              top: 8,
              color: (theme) => theme.palette.grey[500],
            }}
          >
            {/* <CloseIcon /> */}
          </IconButton>
        ) : null}
      </DialogTitle>
    );
  };

  BootstrapDialogTitle.propTypes = {
    children: PropTypes.node,
    onClose: PropTypes.func.isRequired,
  };


  return (
    <div>
      <Button variant="outlined" onClick={handleClickOpen}>
        Programare
      </Button>
      <BootstrapDialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <BootstrapDialogTitle id="customized-dialog-title" onClose={handleClose}>
          ID Imobil:{imobil.id}
        </BootstrapDialogTitle>
        <DialogContent dividers>
          {/* Descriere */}
          <div style={{ display: 'flex' }}>
            <h1>Descriere:</h1>
            <h1>{imobil.descriere}</h1>
          </div>
          {/* Suprafata */}
          <div style={{ display: 'flex' }}>
            <h1>Suprafata:</h1>
            <h1>{imobil.suprafata} mp</h1>
          </div>
          {/* Email Agentie */}
          <div style={{ display: 'flex' }}>
            <h1>emailAgentie:</h1>
            <h1>{imobil.emailAgentie}</h1>
          </div>
          {/* Telefon Agentie */}
          <div style={{ display: 'flex' }}>
            <h1>telefonAgentie:</h1>
            <h1>{imobil.telefonAgentie}</h1>
          </div>
          {/* Telefon Proprietar */}
          <div style={{ display: 'flex' }}>
            <h1>telefonProprietar:</h1>
            <h1>{imobil.telefonProprietar}</h1>
          </div>
          {/* Email Proprietar */}
          <div style={{ display: 'flex' }}>
            <h1>emailProprietar:</h1>
            <h1>{imobil.emailProprietar}</h1>
          </div>
          {/* Adresa */}
          <div style={{ display: 'flex' }}>
            <h1>adresa:</h1>
            <h1>{imobil.adresa}</h1>
          </div>
          {/* Status */}
          <div style={{ display: 'flex' }}>
            <h1>Status:</h1>
            <h1>{imobil.status}</h1>
          </div>
          {/* Tip */}
          <div style={{ display: 'flex' }}>
            <h1>Tip:</h1>
            <h1>{imobil.tip}</h1>
          </div>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>

            <KeyboardDateTimePicker
              id="time-picker"
              label="Time picker"
              value={selectedDate}
              onChange={handleDateChange}
            />

          </MuiPickersUtilsProvider>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose}>
            Save changes
          </Button>
          <Button autoFocus onClick={inchidere}>
            Inchidere
          </Button>
        </DialogActions>
      </BootstrapDialog>
    </div>
  )
}