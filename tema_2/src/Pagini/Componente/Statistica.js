import React, { useEffect,useState } from 'react';
import { useLocation } from 'react-router-dom'
import { HOST } from '../../links.js'
import CanvasJSReact from './canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default function Statistica(props){

    const location=useLocation();// Folosit pentru a citi id-ul imobilului pentru care se fac programari
    const[entry,setEntry]=useState([]);// Datele care se pun pe grafic
    let days=[]// Zilele stocate ca si date
    let count=[]// Numarul de programari pentru fiecare data
    
    useEffect( async ()=>{
        
        const idImobil=location.state.userId;//Id-ul imobilului pentru care ma uit la programari
        days=[] // Array in care am fiecare zi din ultimele 30
        count=[] // Numarul de programari din fiecare zi din ultimele 30
        
        fetch(HOST.backend_api + 'ultimeleProgramari/' + idImobil, {
            method: 'GET',
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },

        })
            .then(response => response.json())
            .then(text => {
                
                // Prelucrez datele venite din back-end si le sparg in zile si numar programari
                // Format venit din back-end: Y-d-m|count
                for(var i=0;i<text.length;i++){
                    let res=text[i].split("|");
                    days.push(res[0])
                    count.push(res[1])
                }
               
                var dp=[]// Datele pe care le pun pe grafic
                // Prelucrare date pentru a le pune pe grafic
                // !!!!!! NUMERE PENTRU AXA Y
                for(var i=0;i<days.length;i++){
                    dp.push({y:parseInt(count[i]),label:days[i]})
                }
                setEntry(dp)
                
            });


    },[])
    
    

    const options = {
        title: {
          text: "Vizitele pentru ultimele 30 de zile"
        },
        data: [{				
                  type: "line",
                  dataPoints:entry
         }]
     }
   
    return(
        <>
            <h1> Statistica programarilor pentru imobil</h1>
            <CanvasJSChart options = {options}
            /* onRef = {ref => this.chart = ref} */
            />
            
        </>
    )
}

