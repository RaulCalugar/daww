import React, { useState,useEffect } from 'react'
import xmlData from './stiri.xml'
import Noutati from '../Noutati'
import '../Styles/Stiri.css'
function Stiri() {

  const[stiri,setStiri]=useState([])
  useEffect(()=>{
    let xml=''
    fetch(xmlData).then((res)=>{
      res.text().then((xml)=>{
        let parser=new DOMParser();//Parser
        let xmlDOM=parser.parseFromString(xml,'application/xml')
        console.log('console:\n',xmlDOM)
        //In stiri am toate stirile din fisierul xml
        let stiriXML=xmlDOM.querySelectorAll('stire')
        setStiri(Object.values(stiriXML))
      })
    })
  },[])    

  return (
    <div>
     <h1 class="titluNoutati">
                Aici gasiti cele mai noi stiri de pe piata imobiliara:
            </h1>
      {stiri.map(stire=><Noutati stire={stire}/>)}
      {/* <h1>{stiri.map(stire=>stire.children[1].firstChild.data)}</h1> */}
    </div>
  )
}

export default Stiri