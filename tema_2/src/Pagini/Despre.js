import React from 'react'
import './Styles/Despre.css'
import Meniu from './Meniu'
import videoFirma from './Video/VideoPromo.mp4'
import poster from './Imagini/pozaCasa.jpg'
import {useTranslation} from 'react-i18next'


function Despre() {

   const{t,i18n}=useTranslation(); 

  return (
    <div>
        <Meniu/>
        {/* Titlul paginii denumit despre noi */}
        <h1 className="titlu">{t('despreNoi')}</h1>
    <p className="descriereDespre">
       
    {t('despre')}
    </p>

    {/* <!-- Video de prezentare al firmei --> */}
    <div className="filmPrezentare">
        <div className="containerVideo">
             {/* <iframe width="560" height="315" src="https://www.youtube.com/embed/DMSrkkijflg" title="YouTube video player" 
                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
                allowfullscreen>  */}
                <video  height="315" controls poster={poster}>
                    <source src={videoFirma} type="video/mp4"/>
                </video>
            
            {/* </iframe> */}
        </div>
    </div>
   

    {/* <!-- Imaginile firmei --> */}
    <div className="imagini">
        <div className="imagine">
            <img src="./Imagini/bloc1.jpg" alt="" classNameName="imagineAnunt"/>
        </div>
        <div className="imagine">
            <img src="./Imagini/bloc2.jpg" alt="" classNameName="imagineAnunt"/>
        </div>
        <div className="imagine">
            <img src="./Imagini/bloc3.jpg" alt="" classNameName="imagineAnunt"/>
        </div>
    </div>


    </div>
  )
}

export default Despre