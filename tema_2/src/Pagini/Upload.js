import React,{useState} from 'react';
import {Checkbox} from '@material-ui/core'
import Meniu from './Meniu';
import './Styles/Upload.css'
import { HOST } from '../links';
import axios from 'axios';

export default function Upload()
{

    const[bifat,setBifat]=useState(false);
    const[fisier,setFisier]=useState("")

    async function uploadFisier()
    {
        const formData=new FormData();
        if(bifat===false){
            alert("Trebuie sa fiti de acord cu prelucrarea datelor")
        }
        else{

            formData.append('fisier',fisier);
            formData.append('nume','raul')
            
            axios({
                method: 'post', // Declare the method
                url: HOST.backend_api + 'uploadPdf',
                data: formData,
                config: { headers: {'Content-Type': 'multipart/form-data' }} // declare the kind of data
            })
                .then( response=> {
                    if(response.data.mesaj==='succes')
                    {
                        alert("Fisier postat cu succes")
                    }
                    else{
                        alert("Eroare la postarea fisierului")
                    }
                   
                })
                .catch(function (response) {
                    console.log(response); // something went wrong
                });
            
        }

        



    }

    return(
        <>
            <Meniu/>
         
            <br/>
            <input type="file"  className="form-control" onChange={(e)=>setFisier(e.target.files[0])} encType="multipart/form-data" />
            
            <div className='divs'>
            <h1 className='gdpr'>Sunteti de acord ca datele dumneavoastra sa fie prelucrate folosind GDPR?</h1>
                <h1 className='stea'>*</h1>
                <Checkbox
                    checked={bifat}
                    onChange={e=>setBifat(e.target.checked)}
                    inputProps={{ 'aria-label': 'controlled' }}
                />
            </div>
            <button onClick={uploadFisier}>Upload Fisier</button>
            

        </>
    )
}

