import React,{useState} from 'react'
import Meniu from './Meniu'
import './Styles/Contact.css'


export default function Contact() {

    const[email,setEmail]=useState("");//Email-ul celui care trimite
    const[mesaj,setMesaj]=useState("");//Textul mesajului 


    async function trimitereMail() {
        const body={email,mesaj}
        await fetch('http://localhost:5000/trimitereMail', {
                      method: 'POST',
                      headers: {'Content-Type': 'application/json'},
                      body: JSON.stringify(body),
                    })
                    .then(response => response.text())
                    .then(text => {console.log('The response was....', text);});
                    
    }
  return (
     <>
        <Meniu></Meniu>
        {/* Card ce contine detalii despre firma */}
       <div className="cardContact">
        <h1 className="titlu">
            Agentie Imobiliara
        </h1>
        <h1 className="numeAgentie">
            S.C IDEAL IMOBILIARE S.R.L
        </h1>
        <h1 className="numarTelefon">
            Numar Telefon Agentie: <h1 className="numarTelefonAgentie">0743894736</h1>
        </h1>
        <h1 className="locatie">
            Strada Padurii, numarul 10, Cluj-Napoca
        </h1>
        <h1 className="email">
            Email: <h1 className="emailAgentie">raul.calugar@yahoo.ro</h1>

        </h1>
    </div>
    <div className="trimitereEmail">
        {/* <!-- Trimitere Email --> */}
        <div className="campEmail">
            <label for="email" className="etichetaEmail">Email-ul dumneavoastra: </label>
            <input type="email" className="inputEmail" id="email" name="email" onChange={e=>setEmail(e.currentTarget.value)}/> 
        </div>
        {/* <!-- Continut Mesaj --> */}
        <div className="campMesaj">
            <label for="mesaj" className="etichetaMesaj">Mesajul Dumneavoastra</label>
            <textarea  className="textMesaj" id="mesaj" name="mesaj" onChange={e=>setMesaj(e.currentTarget.value)}></textarea>
        </div>
        {/* <!-- Butonul --> */}
        <div className="butonTrimitere">
            <button className="buton" onClick={trimitereMail}>Trimitere Mesaj</button>
        </div>
    </div>
     </>
  )
}

