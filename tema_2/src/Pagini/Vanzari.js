import React from 'react'
import './Styles/Vanzari.css'

function Vanzari() {
  return (
    <>
        <h1 class="titluVanzari">
        Pagina Vanzari Imobiliare
    </h1>
    {/* <!-- Partea unde sunt garsoniere de vanzare --> */}
    <div id="garsoniere">
        <h1 class="titluGarsoniere">Vedeti Lista Garsonierelor de Vanzare</h1>  
        {/* <!-- Anunturi garsoniere de vanzare --> */}
        <div class="anunt">
            <img src="./Imagini/Garsoniere/g1.jpg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand garsoniera de 11 mp in Cluj-Napoca, strada Zambilei.Pret 500 euro</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Garsoniere/g2.jpeg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand garsoniera de 25 mp in Cluj-Napoca, strada Obsevatorului</p>
            </div>
        </div>
    </div>

    {/* <!-- Partea unde sunt apartamente de vanzare --> */}
    <div id="apartamente">
        <h1 class="titluApratamente">
            Vedeti lista apartamentelor de vanzare
        </h1>
        {/* <!-- Anunturi apartamente de vanzare --> */}
        <div class="anunt">
            <img src="./Imagini/Apartamente/a1.jpg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand apartament in Busteni. Detalii la numarul de telefon 0711223344</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Apartamente/a2.jpeg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand apartament in Busteni. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Apartamente/a3.jpg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand apartament in Busteni. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
    </div>

    {/* <!-- Partea unde sunt case de vanzare --> */}
    <div id="case">
        <h1 class="titluCase">
            Vedeti Lista Caselor de vanzare
        </h1>
        <div class="anunt">
            <img src="./Imagini/Case/c1.jpg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Case/c2.jpeg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>
        <div class="anunt">
            <img src="./Imagini/Case/c3.jpeg" alt="" className="imagineAnunt"/>
            <div class="continutAnunt">
                <h1 class="autor">Autor:Ion Popescu</h1>
                <p class="descriereAnunt">Vand casa in Sibiu. Detalii la numarul de telefon 071122334</p>
            </div>
        </div>

    </div>


    </>
  )
}

export default Vanzari