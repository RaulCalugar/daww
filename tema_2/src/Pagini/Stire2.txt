Anul trecut, agenții din cadrul rețelei locale au intermediat aproximativ 10.000 de tranzacții imobiliare,
 acestea depășind 300 de milioane de euro.
RE/MAX România, parte a rețelei internaționale de francize imobiliare, a anunțat
o creștere de circa 92% a comisioanelor generate în 2021 comparativ cu anul 
precedent, respectiv de la 5,5 la 10,5 milioane de euro. Compania a intermediat în același interval aproximativ 
10.000 de tranzacții imobiliare, care au însumat peste 300 milioane de euro, 
iar pentru anul curent vizează atingerea unei cifre de afaceri de 15 milioane 
de euro, respectiv o creștere accelerată a numărului de francize și agenți.

