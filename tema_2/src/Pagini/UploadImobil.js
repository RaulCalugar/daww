import React, { useState } from 'react';
import './Styles/UploadImobil.css'
import axios from 'axios';
import { HOST } from '../links';
import Meniu from './Meniu';
const optiuni = {
    vanzare: 'Vanzare',
    inchiriere: 'Inchiriere'
}

const agentie = {
    email: 'raul.calugar@yahoo.ro',
    telefon: '0743894736'
}
export default function UploadImobil() {

    const [tip, setTip] = useState("garsoniera")
    const [descriere, setDescriere] = useState("")
    const [suprafata, setSuprafata] = useState(Number)
    const [emailProprietar, setEmailProprietar] = useState("")
    const [numarTelefonProprietar, setNumarTelefonProprietar] = useState("")
    const [adresa, setAdresa] = useState("")
    const [poza, setPoza] = useState("")
    const [status, setStatus] = useState("")

    async function uploadImobil(e)
    {
        e.preventDefault()
        console.log(tip+'\n'+descriere+'\n'+suprafata+'\n'+emailProprietar+'\n'+numarTelefonProprietar+'\n'+adresa+'\n'+status+'\n')
        
        const formData=new FormData()
        formData.append('tip',tip)
        formData.append('descriere',descriere)
        formData.append('suprafata',suprafata)
        formData.append('telefonAgentie',agentie.telefon)
        formData.append('emailAgentie',agentie.email)
        formData.append('numarTelefonProprietar',numarTelefonProprietar)
        formData.append('emailProprietar',emailProprietar)
        formData.append('adreasa',adresa)
        formData.append('poza',poza)
        formData.append('adresa',adresa)
        formData.append('status',status)

        axios({
            method: 'post', // Declare the method
            url: HOST.backend_api + 'uploadImobil',
            data: formData,
            config: { headers: {'Content-Type': 'multipart/form-data' }} // declare the kind of data
        })
            .then( response=> {
               console.log(response)
               alert('Imobil postat cu succes')
            })
            .catch(function (response) {
                console.log(response); // something went wrong
            });
    }

    return (
        <>
        <Meniu/>
            <div className="container">
                <form>
                    {/* Tip Imobil */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="tip">Tip Imobil</label>
                        </div>
                        <div className="col-75">
                            <select id="tip" defaultValue={"garsoniera"} value={tip} name="tip" onChange={e=>setTip(e.currentTarget.value)}>
                                <option value="garsoniera">Garsoniera</option>
                                <option value="apartament">Apartament</option>
                                <option value="casa">Casa</option>
                            </select>
                        </div>
                    </div>
                    {/* Descriere */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="descriere">Descriere</label>
                        </div>
                        <div className="col-75">
                            <textarea id="descriere" name="descriere" placeholder="Descriere" onChange={e=>setDescriere(e.currentTarget.value)}/>
                        </div>
                    </div>
                    {/* Suprafata */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="suprafata">Suprafata</label>
                        </div>
                        <div className="col-75">
                            <input type="number" id="suprafata" name="suprafata" placeholder="Suprafata" onChange={e=>setSuprafata(e.currentTarget.value)}/>
                        </div>
                    </div>

                    {/* Numar Proprietar */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="numarProprietar">Numar Proprietar</label>
                        </div>
                        <div className="col-75">
                            <input type="text" id="numarProprietar" name="numarProprietar" placeholder="Numar Proprietar" 
                            onChange={e=>setNumarTelefonProprietar(e.currentTarget.value)}/>
                        </div>
                    </div>
                    {/* Email Proprietar */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="emailProprietar">Email Proprietar</label>
                        </div>
                        <div className="col-75">
                            <input type="text" id="emailProprietar" name="emailProprietar" placeholder="Email Proprietar" 
                             onChange={e=>setEmailProprietar(e.currentTarget.value)}
                            />
                        </div>
                    </div>
                    {/* Adresa Imobil */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="adresa">Adresa</label>
                        </div>
                        <div className="col-75">
                            <input type="text" id="adresa" name="adresa" placeholder="Adresa" 
                            onChange={e=>setAdresa(e.currentTarget.value)}/>
                        </div>
                    </div>
                    {/* Poza */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="poza">Poza</label>
                        </div>
                        <div className="col-75">
                            <input type="file" id="poza" name="poza" onChange={e=>setPoza(e.target.files[0])}/>
                        </div>
                    </div>

                    {/* Status Imobil: daca este sau nu  de vanzare */}
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="tip">Status</label>
                        </div>
                        <div className="col-75">
                            <select id="tip" defaultValue={optiuni.vanzare} value={status} name="tip" onChange={e=>setStatus(e.currentTarget.value)}>
                                <option value={optiuni.vanzare}>{optiuni.vanzare}</option>
                                <option value={optiuni.inchiriere}>{optiuni.inchiriere}</option>
                                
                            </select>
                        </div>
                    </div>
                    {/* Buton Incarcare */}
                    <div className='row2'>
                        <button className='submit2' onClick={uploadImobil}>Incarca</button>
                    </div>
                </form>
            </div>
        </>
    )



}


