import React, { useState, useEffect } from 'react'
import Container from "@material-ui/core/Container";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { Grid } from "@material-ui/core";
import './Styles/Logare.css'
import { HOST } from '../links';
import { useNavigate } from 'react-router-dom'

function Logare() {
    const [nume, setNume] = useState("");
    const [parola, setParola] = useState("")
    const [email, setEmail] = useState("");
    const navigate = useNavigate()

    // Vedem daca utilizatorul este logat, atunci numai poate accesa pagina de 
    // logare si merge la pagina de acasa
    useEffect(() => {
        if (localStorage.getItem('userLogat')) {
            navigate('/')
        }
    }, [])


    async function onSubmitFun(ev) {
        ev.preventDefault()
        console.log(nume)
        console.log(parola)
        const body={nume,parola}
        let res = await fetch(HOST.backend_api + 'login', {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        })

        res = await res.json();
        console.log(res)
        if(res.mesaj==='eroare'){
            alert("Nume sau parola gresite")
        }
        else{
            
            //Pun pe localStorage pentru a vedea ca am utilizator logat
            localStorage.setItem("userLogat", JSON.stringify(res))
            //Merg la pagina de home
            navigate('/')
        }
    }


    return (
        <>

            <Container maxWidth="xs" >
                <form onSubmit={onSubmitFun}>
                    <Grid container spacing={3}>
                        <Grid item xs={12}>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="nume"
                                        name="nume"
                                        size="small"
                                        variant="outlined"
                                        onChange={e => setNume(e.currentTarget.value)}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth
                                        label="parola"
                                        name="parola"
                                        size="small"
                                        type="password"
                                        variant="outlined"
                                        onChange={e => setParola(e.currentTarget.value)}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={12}>
                            <Button color="secondary" fullWidth type="submit" variant="contained">
                                Logare
                            </Button>
                            {/* <Button color={"secondary"} fullWidth variant={"contained"} onClick={() => this.props.history.push("/parolaUitata")}>
                            Parola Uitata
                        </Button> */}
                        </Grid>
                    </Grid>
                </form>
            </Container>
        </>
    )
}

export default Logare    